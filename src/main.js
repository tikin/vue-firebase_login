import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import firebase from "firebase";

let app = "";

Vue.config.productionTip = false;

var firebaseConfig = {
  apiKey: "AIzaSyBymERWP6kkJXFHvD8aQxQ_iXcZVp0U6dY",
  authDomain: "vue-firebase-tutorial-25d79.firebaseapp.com",
  databaseURL: "https://vue-firebase-tutorial-25d79.firebaseio.com",
  projectId: "vue-firebase-tutorial-25d79",
  storageBucket: "vue-firebase-tutorial-25d79.appspot.com",
  messagingSenderId: "85103455892",
  appId: "1:85103455892:web:19ccadd413dc7fb95d4e81"
};

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount("#app");
  }
});
